const dropdowns =  document.getElementsByClassName("project-dropdown-container");
const dropdownIcons = document.getElementsByClassName("project-dropdown");
const numberOfDropdowns = dropdowns.length;

for(let index = 0; index < numberOfDropdowns; index++){

    dropdowns[index].addEventListener("click", function (){
        if(dropdownIcons[index].classList.contains("fa-chevron-down")){
            dropdownIcons[index].classList.remove("fa-chevron-down");
            dropdownIcons[index].classList.add("fa-chevron-up");
        }
        else{
            dropdownIcons[index].classList.remove("fa-chevron-up");
            dropdownIcons[index].classList.add("fa-chevron-down");
        }
    });
}



const wh = window.innerHeight; // as soon as element touches bottom with offset event starts
let scrollpos;

const skills = document.getElementsByClassName("skill");
const simplePortfolio = document.getElementById("simple-portfolio-div");

window.addEventListener('scroll', function(){
    scrollpos = window.scrollY;

    for(let i = 0; i < skills.length; i++) {
        if (scrollpos >= (skills[i].offsetTop - wh)) {
            skills[i].classList.add("animate__animated");
            skills[i].style.visibility = "visible";
            skills[i].classList.add("animate__fadeIn");
        }
    }

    if (scrollpos >= (simplePortfolio.offsetTop - wh)) {
        simplePortfolio.classList.add("animate__fadeIn");
        simplePortfolio.classList.add()
    }

});

window.addEventListener("load", function (){
    const switcher = document.getElementById("switcher");
    switcher.addEventListener("change", handleSwitcher);
});

function handleSwitcher(){
    const switcher = document.getElementById("switcher");
    if(switcher.checked === false)
        showSkillsList();
    else
        showExtendedPortfolio();
}

function showSkillsList(){
    setActiveSpan("switcher-skills");
    document.getElementById("simple-portfolio-div").style.display = "block";
    document.getElementById("extended-portfolio-div").style.display = "none";
}

function showExtendedPortfolio(){
    setActiveSpan("switcher-extended");
    document.getElementById("simple-portfolio-div").style.display = "none";
    document.getElementById("extended-portfolio-div").style.display = "block";
}

function setActiveSpan(id){
    const activeSpan = document.getElementsByClassName("active-switch")[0];
    const newActiveSpan  = document.getElementById(id);

    if(!activeSpan || !newActiveSpan)
        return;

    activeSpan.classList.remove("active-switch")
    newActiveSpan.classList.add("active-switch");
}
