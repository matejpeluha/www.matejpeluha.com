const wh = window.innerHeight-50; // as soon as element touches bottom with offset event starts
let scrollpos;

const university = document.getElementById("universityAnimatedPart"); //element
const universityIng = document.getElementById("universityAnimatedPartIng");

window.addEventListener('scroll', function(){
    scrollpos = window.scrollY;

    if(scrollpos > (university.offsetTop - wh)){
        university.style.visibility = "visible";
        university.classList.add("animate__animated");
        university.classList.add("animate__fadeIn");
        university.classList.add("animate__slower");
    }
    if(scrollpos > (universityIng.offsetTop - wh)){
        universityIng.style.visibility = "visible";
        universityIng.classList.add("animate__animated");
        universityIng.classList.add("animate__fadeIn");
        universityIng.classList.add("animate__slower");
    }
});

const masterButton = document.getElementById("master-degree");
const bachelorButton = document.getElementById("bachelor-degree");

masterButton.addEventListener("click", function (){
    masterButton.classList.add("active-degree");
    bachelorButton.classList.remove("active-degree");
    universityIng.style.display = "block";
    university.style.display = "none";


})

bachelorButton.addEventListener("click", function (){
    masterButton.classList.remove("active-degree");
    bachelorButton.classList.add("active-degree");
    universityIng.style.display = "none";
    university.style.display = "block";


})
