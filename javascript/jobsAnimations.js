const wh = window.innerHeight-50; // as soon as element touches bottom with offset event starts
let scrollpos;
const workLeft = document.getElementsByClassName("workLeft"); //element



window.addEventListener('scroll', function(){
    scrollpos = window.scrollY;

    for(let i = 0; i < workLeft.length; i++) {
        if (scrollpos > (workLeft[i].offsetTop - wh)) {
            workLeft[i].style.visibility = "visible";
            workLeft[i].classList.add("animate__animated");
            workLeft[i].classList.add("animate__fadeInRight");
            workLeft[i].classList.add("animate__slower");
        }
    }

});


const workRight = document.getElementsByClassName("workRight"); //element

window.addEventListener('scroll', function(){
    scrollpos = window.scrollY;
    for(let i = 0; i < workRight.length; i++) {
        if (scrollpos > (workRight[i].offsetTop - wh)) {
            workRight[i].style.visibility = "visible";
            workRight[i].classList.add("animate__animated");
            workRight[i].classList.add("animate__fadeInLeft");
            workRight[i].classList.add("animate__slower");
        }
    }
});