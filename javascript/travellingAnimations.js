const countries = document.getElementsByClassName("country");
const carousels = document.getElementsByClassName("slideShow");
const countryHeader = document.getElementById("countryName");

for(let i = 0; i < countries.length; i++){

    countries[i].addEventListener("click", function (){
        for(let index = 0; index < countries.length; index++){
            countries[index].classList.remove("activeCountry");
            if(index === i){
                countries[index].classList.add("activeCountry");
            }
        }

        for(let index = 0; index < carousels.length; index++){
            if(index === i){
                carousels[index].classList.remove("hiddenSlideshow");
                carousels[index].classList.add("activeSlideShow");
                carousels[index].classList.remove("animate__fadeOut");
                carousels[index].classList.add("animate__fadeIn");
                countryHeader.innerText = countries[i].getAttribute("hname");
            }
            else{
                carousels[index].classList.add("hiddenSlideshow");
                carousels[index].classList.remove("activeSlideShow");
                carousels[index].classList.add("animate__fadeOut");
                carousels[index].classList.remove("animate__fadeIn");
            }
        }
    })
}




function setVisible(selector, visible) {
    document.querySelector(selector).style.display = visible ? 'block' : 'none';
}


window.addEventListener('load', function () {
    setVisible('.page', true);
    setVisible('#loading', false);
})

